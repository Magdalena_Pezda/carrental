/*
 * Menu.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include <iostream>
#include "Menu.h"

Menu::Menu() {
	carRental = 0;
}
Menu::Menu(CarRental *carRental) :
		carRental(carRental) {
}

Menu::~Menu() {
	// TODO Auto-generated destructor stub
}
void Menu::showMenu() {
	std::cout << "How can we help you?\n"
			"1) Show all cars\n"
			"2) Find preffered car\n"
			"3) Rent a car\n"
			"4) Return car\n"
			"5) Add new car\n"
			"6) Contact us\n"
			"7) Exit\n";
}
int Menu::chooseOption() {
	int option(0);
	std::cout << "Choose option: \n";
	std::cin >> option;
	return option;
}
void Menu::printAllCars() {
	std::cout << "All cars:\t\tAvailability:\n";
	for (int i = 0; i < carRental->cars.size(); ++i) {
		std::cout << i + 1 << ") ";
		carRental->cars[i].printCar();
		std::cout << "\t\t";
		if (carRental->cars[i].isIsAvailable())
			std::cout << "Available\n";
		else
			std::cout << "Rented\n";
	}
}
void Menu::performTask(int task) {
	switch (task) {
	case 1:
		printAllCars();
		enterToContinue();
		break;
	case 2:{
		std::vector<Car*> preferredCars = findPrefferedCars();
		printPreferredCars(preferredCars);
		enterToContinue();
		break;
	}
	case 3:
		rentObject();
		enterToContinue();
		break;
	case 4:
		returnCar();
		enterToContinue();
		break;
	case 5:
		addNewCar();
		enterToContinue();
		break;
	case 6:
		printCarRenthalContact();
		enterToContinue();
		break;
	case 7:
		std::cout << "Thank you for visit! See you soon!\n	";
		break;
	default:
		std::cout << "Invalid option chosen\n";
		enterToContinue();
	}
}
int Menu::chooseCar() {
	std::cout << "Enter car number to choose: ";
	int carNumber;
	std::cin >> carNumber;
	return carNumber;
}
void Menu::rentObject() {
	printAllCars();
	int carNumber = chooseCar();
	if (carRental->rentCar(carNumber)) {
		double price = priceToPayForRent(carNumber);
		std::cout << "Amount to pay: " << price << " zl\n";
		carRental->cars[carNumber - 1].printCar();
		std::cout << "You've rent ";
		std::cout << "!";
	} else
		std::cout << "Chosen car is unavailable.\n";
}
void Menu::runSystem() {
	std::cout << "Welcome to M&M Car Rental!\n";
	int option(0);
	while (option != 7) {
		showMenu();
		option = chooseOption();
		performTask(option);
	}
}
std::string Menu::enterRegistrationNumber() {
	std::cout << "Please enter registration number : ";
	std::string registrationNbr;
	std::cin >> registrationNbr;
	return registrationNbr;
}

bool Menu::returnCar() {
	std::vector<Car> baseCars = carRental->getCars();
	for (std::vector<Car>::iterator it = baseCars.begin(); it != baseCars.end();
			++it)
		if (it->getRegistrationNumber() == enterRegistrationNumber())
			it->setIsAvailable(true);
	return false;
}

void Menu::addNewCar() {
	std::cout << "Please enter brand: ";
	std::string newBrand;
	std::cin >> newBrand;
	for (int i = 0; i < carRental->cars.size(); ++i) {
		if (carRental->cars[i].printBrand() == newBrand)
			break;
		else
			std::cout << "Unavailable model";
			return;
	}
	std::cout << "Please enter model: ";
	std::string newModel;
	std::cin >> newModel;
	for (int i = 0; i < carRental->cars.size(); ++i) {
		if (carRental->cars[i].printModel() == newModel)
			break;
		else
			std::cout << "Unavailable model";
			return;
	}
	std::cout << "Please enter registration number: ";
	std::string newNumber;
	std::cin >> newNumber;
	std::cout << "Please enter price per day: ";
	double newPrice;
	std::cin >> newPrice;
	std::cout << "Please production year: ";
	int newProductionYear;
	std::cin >> newProductionYear;
	carRental->addCar(Car::returnBrand(newBrand), Car::returnModel(newModel),
			newNumber, newPrice, newProductionYear, true);
}
void Menu::enterToContinue() {
	std::cout << "Press ENTER to continue" << std::endl;
	std::cin.get();
	std::cin.get();
}
void Menu::printCarRenthalContact() {
	std::cout << "Any questions? Contact us!\n"
			"eMail: " << carRental->getMail() << "\nTelephone: "
			<< carRental->getPhoneNumber() << '\n';
}

std::vector<Car*> Menu::findPrefferedCars() {
	std::cout << "Production year from: ";
	int from = getYear();
	std::cout << "Production year to: ";
	int to = getYear();
	if(from > to){
		from = from + to;
		to = from - to;
		from = from - to;
	}
	return carRental->findCarByProductionYear(from, to);
}
int Menu::addRentedDays() {
	std::cout << "Days of rent: ";
	int rentedDays;
	std::cin >> rentedDays;
	return rentedDays;
}

double Menu::priceToPayForRent(int carNumber) {
	int days  = addRentedDays();
	double dailyPrice = carRental->cars[carNumber - 1].getPriceForDay();
	return days*dailyPrice;
}
int Menu::getYear() {
	int year;
	std::cin >> year;
	return year;
}

void Menu::printPreferredCars(std::vector<Car*> preferredCars) {
	if (preferredCars.size() > 0) {
		std::cout << "Preferred cars:\t\tAvailability:\n";
		for (int i = 0; i < preferredCars.size(); ++i) {
			std::cout << i + 1 << ") ";
			carRental->cars[i].printCar();
			std::cout << "\t\t";
			if (carRental->cars[i].isIsAvailable())
				std::cout << "Available\n";
			else
				std::cout << "Rented\n";
		}
	} else
		std::cout << "We're sorry, there is no car matching your preferences.\n";
}
