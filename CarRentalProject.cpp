//============================================================================
// Name        : CarRentalProject.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================


#include <iostream>
#include "Car.h"
#include "BaseInterface.h"
#include "Menu.h"
#include "gtest/gtest.h"



int main(int argc, char **argv) {
//	::testing::InitGoogleTest(&argc, argv);

	Car ToyotaYaris(Car::Toyota, Car::Yaris, "KR12345", 100, 2016);
	Car ToyotaYaris2(Car::Toyota, Car::Yaris, "KR98765", 100, 2015, 0);
	Car HondaAccord(Car::Honda, Car::Accord, "KR54356", 120, 2017);
	Car HondaCivic(Car::Honda, Car::Civic, "KR98555", 150, 2017);

	CarRental carRental("790 111 111", "MMCarRenthal@gmail.com");
	carRental.addCar(ToyotaYaris);
	carRental.addCar(ToyotaYaris2);
	carRental.addCar(HondaAccord);
	carRental.addCar(HondaCivic);

	Menu menu(&carRental);
	menu.runSystem();

//	return RUN_ALL_TESTS();
	return 0;
}
