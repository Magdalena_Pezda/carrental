/*
 * Car.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef CAR_H_
#define CAR_H_
#include <string>


class Car {

public:
	enum brand {Toyota, Fiat, Opel, Honda};
	enum model {Yaris, Corolla, Aygo, Punto, Panda, Astra, Vectra, Accord, Civic, Jazz};
	Car();
	Car(brand brand_, model model_, std::string registrationNumber_, double priceForDay_, int productionYear_, bool isAvailable_=1);
	virtual ~Car();
	brand getCarBrand() const {
		return carBrand;
	}
	model getCarModel() const {
		return carModel;
	}
	bool isIsAvailable() const {
		return isAvailable;
	}
	void setIsAvailable(bool isAvailable) {
		this->isAvailable = isAvailable;
	}
	double getPriceForDay() const {
		return priceForDay;
	}
	int getProductionYear() const {
		return productionYear;
	}
	const std::string& getRegistrationNumber() const {
		return registrationNumber;
	}
	int getRentedDays() const {
		return rentedDays;
	}
	void printCar();
	static Car::brand returnBrand (std::string brand_);
	static Car::model returnModel (std::string model_);
	std::string printBrand();
	std::string printModel();

private:
	std::string registrationNumber;
	double priceForDay;
	int productionYear;
	bool isAvailable;
	brand carBrand;
	model carModel;
	int rentedDays;
};

#endif /* CAR_H_ */
