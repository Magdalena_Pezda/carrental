/*
 * CarRental.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "CarRental.h"

CarRental::CarRental() {
}
CarRental::CarRental(std::string phoneNumber, std::string eMail) :
		phoneNumber(phoneNumber), eMail(eMail) {
}
CarRental::~CarRental() {
	// TODO Auto-generated destructor stub
}

bool CarRental::addCar(Car::brand brand_, Car::model model_,
		std::string registrationNumber_, double priceForDay_,
		int productionYear_, bool isAvailable_) {
	Car car(brand_, model_, registrationNumber_, priceForDay_, productionYear_);
	if (isRegistrationUnique(&car) == true) {
		cars.push_back(car);
		return true;
	} else
		return false;
}
bool CarRental::isRegistrationUnique(Car *car) {
	for (std::vector<Car>::iterator it = cars.begin(); it != cars.end(); ++it)
		if (it->getRegistrationNumber() == car->getRegistrationNumber())
			return false;
	return true;
}

bool CarRental::addCar(Car car) {
	if (isRegistrationUnique(&car) == true) {
		cars.push_back(car);
		return true;
	} else
		return false;
}

void CarRental::deleteCar(Car car) {
	for (std::vector<Car>::iterator it = cars.begin(); it != cars.end(); ++it) {
		if (it->getRegistrationNumber() == car.getRegistrationNumber())
			cars.erase(it);
	}
}
bool CarRental::rentCar(int index) {
	--index;
	if (cars[index].isIsAvailable() && index >= 0 && index < cars.size()) {
		cars[index].setIsAvailable(false);
		return true;
	} else
		return false;

}

std::vector<Car*> CarRental::findCarByProductionYear(int since, int to) {
	std::vector<Car*> preferredCars;
	for (int i = 0; i < cars.size(); ++i) {
		if (cars[i].getProductionYear() >= since && cars[i].getProductionYear() <= to)
			preferredCars.push_back(&cars[i]);
	}
	return preferredCars;
}
