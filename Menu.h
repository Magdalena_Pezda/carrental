/*
 * Menu.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef MENU_H_
#define MENU_H_

#include "BaseInterface.h"
#include "CarRental.h"

class Menu {
public:
	Menu();
	Menu(CarRental *carRental);
	virtual ~Menu();

	void runSystem();
	void showMenu();
	int chooseOption();
	void rentObject();
	void printAllCars();
	void performTask(int task);
	int chooseCar();
	std::string enterRegistrationNumber();
	void enterToContinue();
	bool returnCar();
	void addNewCar();
	std::string enterBrandCar();
	int addRentedDays();
	double priceToPayForRent(int carNumber);
	void printCarRenthalContact();
	std::vector<Car*> findPrefferedCars();
	void printPreferredCars(std::vector<Car*> preferredCars);
private:
	CarRental *carRental;

	int getYear();

};

#endif /* MENU_H_ */
