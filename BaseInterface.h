/*
 * BaseInterface.h
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#ifndef BASEINTERFACE_H_
#define BASEINTERFACE_H_

class BaseInterface {
public:
	BaseInterface();
	virtual ~BaseInterface();

	virtual void showMenu() = 0;
	virtual void rentObject() = 0;
	virtual void printAvailableCars() = 0;
};

#endif /* BASEINTERFACE_H_ */
