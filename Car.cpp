/*
 * Car.cpp
 *
 *  Created on: 31.05.2017
 *      Author: RENT
 */

#include "Car.h"
#include <iostream>

Car::Car(){
	this->carBrand=Toyota;
	this->carModel=Corolla;
	this->priceForDay=25.50;
	this->productionYear=2016;
	this->isAvailable=true;
	this->rentedDays=0;
}

Car::Car(Car::brand brand_, Car::model model_, std::string registrationNumber_, double priceForDay_,
		int productionYear_, bool isAvailable_) : carBrand(brand_), carModel(model_),
		registrationNumber(registrationNumber_), priceForDay(priceForDay_), productionYear(productionYear_),
		isAvailable (isAvailable_) {
		rentedDays=0;
}
Car::~Car() {
	// TODO Auto-generated destructor stub
}
void Car::printCar() {
	std::string brand[4] = {"Toyota", "Fiat", "Opel", "Honda"};
	std::string model[10] = {"Yaris", "Corolla", "Aygo", "Punto", "Panda", "Astra", "Vectra", "Accord", "Civic", "Jazz"};
	std::cout << brand[this->carBrand] << " " << model[this->carModel];
}

Car::brand Car::returnBrand(std::string brand_) {
	if (brand_ == "Toyota")
		return Car::Toyota;
	else if (brand_ == "Fiat")
		return Car::Fiat;
	else if (brand_ == "Honda")
		return Car::Honda;
	else if (brand_ == "Opel")
		return Car::Opel;
}

Car::model Car::returnModel(std::string model_) {
	if (model_ == "Yaris")
		return Car::Yaris;
	else if (model_ == "Corolla")
		return Car::Corolla;
	else if (model_ == "Aygo")
		return Car::Aygo;
	else if (model_ == "Punto")
		return Car::Punto;
	else if (model_ == "Astra")
		return Car::Astra;
	else if (model_ == "Vectra")
		return Car::Vectra;
	else if (model_ == "Accord")
		return Car::Accord;
	else if (model_ == "Civic")
			return Car::Civic;
	else if (model_ == "Jazz")
			return Car::Jazz;
	else if (model_ == "Panda")
			return Car::Panda;
}

std::string Car::printBrand() {
	std::string brandsArray[4] = {"Toyota", "Fiat", "Opel", "Honda"};
	return brandsArray[this->carBrand];
}

std::string Car::printModel() {
	std::string modelsArray[10] = {"Yaris", "Corolla", "Aygo", "Punto", "Panda", "Astra", "Vectra", "Accord", "Civic", "Jazz"};
	return modelsArray[this->carModel];
}

